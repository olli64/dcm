import numpy as np
from dcm import *
from functions import *
from plotting import *
import pylab as plt

def jacobian(nw,t_end,h1=0.1, h2=0.1,eps=0.1):
    #Numeric solution of neuro_eq
    u=nw.u
    f=lambda t,z : neuro_eq(t,z,u,nw.A,nw.B,nw.C,nw.D)
    neuro_0 = rk.rk4(f,nw.z0,h1,t_end)  
    bold_0  = solve_hdm(neuro_0,h2)[1]
    j=[]
    A=nw.A
    j=np.zeros((nw.A.size,nw.nz*bold_0.shape[1]))
    it = np.nditer(A,flags=['multi_index'])
    i=0
    #plt.plot(bold_0[1],label="bold0")
    for elem in it:
        idx = it.multi_index
        A_st = nw.A.copy()
        A_st[idx] = eps+elem
        #print(A_st)#
        f=lambda t,z : neuro_eq(t,z,u,A_st,nw.B,nw.C,nw.D)
        neuro_st = rk.rk4(f,nw.z0,h1,t_end)  
        bold_st  = solve_hdm(neuro_st,h2)[1]
        dif = (bold_st-bold_0)/(eps)
        j[i]=dif.flatten()
        i+=1
    return (j,bold_0.flatten())
    

def newton_method(y,nw,t_end,h1=0.1, h2=0.1,eps=0.005): #!TODO: t_end aus y
    data_shape = y.shape
    y = y.flatten()
    theta = nw.A.flatten()
    for i in range(10):
        print(nw.A)
        j,y_s = jacobian(nw,t_end,h1, h2,eps)
        dy = y - y_s
        j_inv = np.linalg.inv(j.dot(j.transpose())).dot(j)
        theta = theta + j_inv.dot(dy)
        nw.A = theta.reshape(nw.A.shape)
        if (np.linalg.norm(dy) <1E-4):
            print('Converged!')
            break
    print("Ran into: ",nw.A)
    t = np.linspace(0,t_end,t_end/h2+1)
    plot_bold(t,y.reshape(data_shape),'data')   
    plot_bold(t,y_s.reshape(data_shape),'fit')
        

##numeric paramters
h1=0.05; h2=0.5; t_end=20.
u= lambda t: np.array([gaus_sum(t,3,23)])
nw=network(2,1)
nw.set_A(0,1,0.5)
nw.set_C(0,0,1.)
nw.set_u(u)
t,bold = simulation(nw,t_end,h1,h2,'model1')
bold +=  np.random.random_sample(bold[1].shape)*1E-4
nw.A=np.zeros((2,2))+0.01
newton_method(bold,nw,t_end,h1,h2)