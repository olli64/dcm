import pylab as plt
import numpy as np
import matplotlib.gridspec as gridspec

def plot_stimulus(t_arr,u,name):
    plt.figure(figsize=(20, 4))
    plt.xlim(0,xrange)
    u_arr=[]
    for t_i in t_arr:
        u_arr.append(u(t_i))
    u_arr=np.array(u_arr)
    for i in range(u_arr.shape[1]):
        plt.plot(t_arr,u_arr[:,i], label='u'+str(i))
    plt.legend()
    #plt.show()
    plt.savefig(name+'_u.svg',bbox_inches='tight')
    
def plot_neuro_state(t_arr,z,name):
    plt.figure(figsize=(20, 5))
    #plt.xlim(0,xrange)
    for i in range(z.shape[1]):
        plt.plot(t_arr,z[:,i], label='z'+str(i))
    plt.legend()
    #plt.show()
    plt.savefig(name+'_z.svg',bbox_inches='tight')
    
def plot_bold(t,bold,name='BOLD'):
    #plt.figure(figsize=(8,6))
    #plt.xlim(0,xrange)
    for i, bold_i in enumerate(bold):
        plt.plot(t,bold_i,label='BOLD_'+name+str(i))        
    plt.legend()
    #plt.show()
    plt.savefig(name+'_bold.svg',bbox_inches='tight')
    
def plot_all(u,neuro_sol,bold_list,name):
    t=neuro_sol[0]
    t_bold= bold_list[0]
    t_end=t[-1]
    z=neuro_sol[1]
    plt.figure(figsize=(8, 6.4))
    gs = gridspec.GridSpec(3, 1,height_ratios=[2,3,3])
    #plt.subplot(311)
    plt.subplot(gs[0])
    plt.tight_layout()
    plt.xlim(0,t_end)
    plt.ylim(0,1.1)
    
    u_arr=[]
    
    for t_i in t:
        u_arr.append(u(t_i))
    u_arr=np.array(u_arr)
    for i in range(u_arr.shape[1]):
        plt.plot(t,u_arr[:,i], label='u'+str(i))
        
    plt.legend()     
    #plt.subplot(312)
    plt.subplot(gs[1])
    plt.tight_layout()
    plt.xlim(0,t_end)
    
    for i in range(z.shape[1]):
        plt.plot(t,z[:,i], label='z'+str(i))
        
    plt.legend()
    
    #plt.subplot(313)
    plt.subplot(gs[2])
    plt.tight_layout()
    plt.xlim(0,t_end)
    
    for i,bold in enumerate(bold_list[1]):
        plt.plot(t_bold,bold,label='BOLD'+str(i))        
    plt.legend()
    plt.show()
    plt.savefig('../plots/'+name+'.svg') #,bbox_inches='tight'