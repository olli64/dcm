import dcm
import numpy as np

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / 2. * np.power(sig, 2.))

def step(t,a,b):
    x=0.
    if (a<t<b):
        x=1.
    return x
    
def gaus_sum(t,num,sep):
    t0=1.
    sum=0.
    for i in range(num):
        sum+=gaussian(t,t0+i*sep,25.)
    return sum
##numeric paramters
h1=0.05; h2=0.1; t_end=70

#model 1: linear
u= lambda t: np.array([gaus_sum(t,3,23)])
nw=dcm.network(3,1)
nw.set_A(0,1,0.7)
nw.set_A(1,2,0.7)
nw.set_C(0,0,1.)
nw.set_u(u)
dcm.simulation(nw,t_end,h1,h2,'model1')
#dcm.jacobian(nw,t_end,h1,h2)

##model 2: linear 3 inputs
#def u(t): 
#    return np.array([gaussian (t,1.,10.),
#                     gaussian (t,24.,10.), 
#                     gaussian (t,47.,10.)])
#A=np.array([[-1,0,0.5],[0.5,-1,0], [0,0.5,-1]])
#B=np.zeros((len(u(0)), len(z0), len(z0)))
#C=np.array([[1,0,0],[0,1,0],[0,0,1]])
#dcm.simulation(A,B,C,D,u,z0,t_end,h1,h2,'model2')
#
##billineare Modelle:
##model 3: 
#def u(t): 
#    return np.array([gaus_sum(t,3,20),
#                     step(t,20,30)])
#A=np.array([[-1.,0.,0.],[0.7,-1.,0.], [0.,0.7,-1.]])                    
#B=np.zeros((len(u(0)), len(z0), len(z0)))
#C=np.zeros((len(z0),len(u(0))))
#C[0,0]=1.
#B[1,2,0]=1. # u2 moduliert z0->z2
#dcm.simulation(A,B,C,D,u,z0,t_end,h1,h2,'model3')
#
##model 4
##setup of model paramters
#A=np.array([[-1,0,0],[1./3,-1,1./3], [0,1./3,-1]])
#C=np.array([[1,0],[0,0],[0,0]])
#def u(t):
#    return np.array([gaus_sum(t,3,23),
#                     step (t,20,30)])
#B=np.zeros((len(u(0)), len(z0), len(z0)))
##B-Matrizen: WW von z2 auf z1 zuschalten
#B[1,0,1]=2.5
#dcm.simulation(A,B,C,D,u,z0,t_end,h1,h2,'model4')

#model 5

#nw=dcm.network(3,1)
#nw.set_A(0,1,2./3)
#nw.set_A(0,2,2./3)
#nw.set_A(2,1,-1./3)
#nw.set_C(0,0,1.)
#nw.set_B(1,0,2,-2./3.)
#nw.set_u(u)
#dcm.simulation(nw,t_end,h1,h2,'model5')