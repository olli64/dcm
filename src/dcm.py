import numpy as np
from plotting import plot_all
from scipy.interpolate import interp1d

import rkmethods as rk

class network(object):
    """a neural network with nz nodes and nu inputs. 
    Connections areiven by the matrices A,B,C.D"""
    def __init__(self,nz,nu):
        self.nz=nz
        self.nu=nu
        self.z0=np.zeros(nz)
        self.A=np.diag(-np.ones(nz))
        self.B=None
        self.C=np.zeros((nz,nu))
        self.D=None
    def set_A(self,con_start,con_end, strength):
        self.A[con_end,con_start]=strength
    def set_B(self,u_idx,con_start,con_end, strength):
        if self.B==None:
            self.B=np.zeros((self.nu,self.nz,self.nz))
        self.B[u_idx,con_end,con_start]=strength
    def set_C(self,u_idx,z_idx,strength):
        self.C[z_idx,u_idx]=strength  
    def set_D(self,z_idx,con_start,con_end, strength):
        if self.D==None:
            self.D=np.zeros((self.nz,self.nz,self.nz))
        self.D[z_idx,con_end,con_start]=strength   
    def set_u(self,u):
        self.u=u
    
    
def neuro_eq(t,z,u,A,B,C,D):
    """returns the change rate in every region for a given neural states z"""
    matrix_sum=A.copy() 
    if B!=None:
        for j in range(u(0).shape[0]):
            matrix_sum += u(t)[j]*B[j]
    if D!=None:
        for k in range(z.shape[0]):
            matrix_sum += z[k]*D[k]
    return (np.dot(matrix_sum,z) + np.dot(C,u(t)))

#biophysical paramteres for the HDM
kappa=0.65
gamma=0.41
tau=0.98
alpha=0.32
roh=0.34
V0=0.02  

def hdm_eq(t,hdm_state,z_i_interp):
    """returns the change rate of the Hemodynamic state variables"""
    s=hdm_state[0]
    f=hdm_state[1]
    v=hdm_state[2]
    q=hdm_state[3]
   
    sdot=z_i_interp(t)-kappa*s-gamma*(f-1.)
    fdot=s
    vdot=1./tau*(f-v**(1./alpha))
    qdot=1./tau*(f*E(f,roh)/roh-v**(1./alpha)*q/v)
    
    return np.array([sdot, fdot, vdot, qdot])
    
def E(f,roh):
    """helper function for hdm_eq"""
    return (1.-(1.-roh)**(1./f))
          
def bold(q,v):
    """returns bold signal for a given q and v solved with the hdm""" 
    k1=7.*roh
    k2=2.
    k3=2.*roh-0.2    
    return V0*(k1*(1.-q)+k2*(1.-q/v)+k3*(1.-v))
    
def solve_hdm(neuro_sol,h2):
    #Numeric solution of hdm_eq
    z=neuro_sol[1]
    t_neuro=neuro_sol[0]
    bold_list=[]
    s0=0.; f0=1.; v0=1.; q0=1.;
    hdm0 = np.array((s0, f0, v0, q0))
    for z_idx in range(z.shape[1]):
        z_i_interp=interp1d(t_neuro, z[:,z_idx], bounds_error=False, fill_value=0.)
        f= lambda t,hdm_state : hdm_eq(t,hdm_state,z_i_interp)
        hdm_sol = rk.rk4(f, hdm0, h2, t_neuro[-1])     
        v=hdm_sol[1][:,2]
        q=hdm_sol[1][:,3]
        bold_list.append(bold(q,v))
    return (hdm_sol[0],np.array(bold_list))
    
def simulation(nw,t_end,h1=0.1, h2=0.1,name='sim'):
    """integrates the neuro and hdm equations and plots results"""
    #Numeric solution of neuro_eq
    f=lambda t,z : neuro_eq(t,z,nw.u,nw.A,nw.B,nw.C,nw.D)
    neuro_sol = rk.rk4(f,nw.z0,h1,t_end)
    print(neuro_sol[0])
    bold_list = solve_hdm(neuro_sol,h2)
    plot_all(nw.u,neuro_sol,bold_list,name)
    return bold_list