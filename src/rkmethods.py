# -*- coding: utf-8 -*-
import numpy as np
import pylab as plt

"""solves the ODE y'=f(y,t) with RK Fehlberg 4/5 in the interval [0,t_end]
       f and y0 are allowed to be vectorial and can differ in dimension"""   
def rkf45(f,y0,e_tol,t_end):
    #Preperation
    t=[0]
    y=[y0]
    h=e_tol
    
    while t[-1]<t_end:
        ok=False
        while (not ok):
            k=[]
            k.append(f(t[-1],  
                       y[-1]))
            k.append(f(t[-1]+h/4.,  
                       y[-1]+h/4.*k[0]))
            k.append(f(t[-1]+h*3./8.,
                       y[-1]+h*(3./32.*k[0]+9./32.*k[1])))
            k.append(f(t[-1]+h*12./13., 
                       y[-1]+h*(1932./2197.*k[0]-7200./2197.*k[1]
                       +7296./2197.*k[2]))) 
            k.append(f(t[-1]+h,
                       y[-1]+h*(439./216.*k[0]-8.*k[1]+3680./513.*k[2]
                       -845./4104.*k[3])))
            k.append(f(t[-1]+h*1./2., 
                       y[-1]-h*(8./27.*k[0]+2.*k[1]-3544./2565.*k[2]
                       +1859./4104.*k[3]-11./40.*k[4])) )             
                 
            y4=y[-1]+h*(25./216.*k[0]+1408./2565.*k[2]+2197./4104.*k[3]-1./5.*k[4])
            y5=y[-1]+h*(16./135.*k[0]+6656./12825.*k[2]+28561./56430.*k[3]-9./50.*k[4]+2./55.*k[5])
               
            epsilon=np.linalg.norm(y5-y4)
            if (epsilon<=e_tol):
                y.append(y4)
                t.append(t[-1]+h)
                ok=True
                h=h*(e_tol/epsilon)**0.2
            else:
                h=0.84*h*(e_tol/epsilon)**0.25   
                #print("t=",t[-1],"eps=",epsilon,"h=",h)
    return (t,np.array(y))
    
"""solves the ODE y'=f(y,t) with classic RK4 in the interval 
[0,t_end] f and y0 can be scalar or np.arrays"""
def rk4(f,y0,h,t_end):
    N = t_end/h+1
    t=np.linspace(0,t_end,N)
    y=np.empty((N,len(y0)))
    y[0]=y0
    for i, t_i in enumerate(t[:-1]):
        k1=f(t_i,     y[i])
        k2=f(t_i+h/2.,y[i]+k1*h/2.)
        k3=f(t_i+h/2.,y[i]+k2*h/2.)
        k4=f(t_i+h,   y[i]+k3*h)      
        y[i+1] = (y[i]+h/6.*(k1+2.*k2+2.*k3+k4))
    return (t,y)