import numpy as np

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / 2. * np.power(sig, 2.))

def step(t,a,b):
    x=0.
    if (a<t<b):
        x=1.
    return x
    
def gaus_sum(t,num,sep):
    t0=1.
    sum=0.
    for i in range(num):
        sum+=gaussian(t,t0+i*sep,25.)
    return sum