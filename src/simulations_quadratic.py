import dcm
import numpy as np
import pylab as plt
from plotting import plot_bold

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / 2. * np.power(sig, 2.))

def step(t,a,b):
    x=0.
    if (a<t<b):
        x=1.
    return x
    
def gaus_sum(t,num,sep,t0=1.):
    sum=0.
    for i in range(num):
        sum+=gaussian(t,t0+i*sep,25.)
    return sum

##numeric paramters
h1=0.01; h2=0.1; 

t_end=60
u=lambda t: np.array([gaus_sum(t,3,20),
                     step(t,20,30)])
# Bil. Beispiel                 
nw=dcm.network(2,2)
nw.set_u(u)
nw.set_A(0,1,0.7)         
nw.set_C(0,0,1.)
nw.set_B(1,0,1,1.)
#nw.set_u(u)       
#data=dcm.simulation(nw,t_end,h1,h2,'1-bil_bsp')   

#plt.xlim(0,60)
#plt.plot(data[1][0][0],data[1][0][1], 'b', data[1][1][0], data[1][1][1], 'g')


# Quadr. Beispiel

nw=dcm.network(3,2)
nw.set_u(u)
nw.set_A(0,1,0.7)         
nw.set_C(0,0,1.)
nw.set_C(1,2,0.05)
nw.set_D(2,0,1,50.)
#nw.set_u(u)       
#data=dcm.simulation(nw,t_end,h1,h2,'1-quadr_bsp')  


#plt.xlim(0,60)
#plt.plot(data[1][0][0],data[1][0][1], 'b', data[1][1][0], data[1][1][1], 'g') 

t_end=180
u= lambda t:np.array([gaus_sum(t,3,32,2)+gaus_sum(t,2,30.5,96.5),
                     gaus_sum(t,5,30,7)])
nw=dcm.network(4,2)
nw.set_u(u)
nw.A=np.array([[-1.,0. ,0., 0.],
               [0. ,-1.,0., 0.], 
               [0.2,0.2,-1.,0.],
               [0.2,0.2,0.,-1.]])
nw.set_C(0,0,1.)
nw.set_C(1,1,1.)
nw.set_D(2,3,0,138)
nw.set_D(3,2,1,138)
dcm.simulation(nw,t_end,h1,h2,'quadr_1')


u=lambda t: np.array([gaus_sum(t,10,4),step(t,25,60)+step(t,60,82)])
nw=dcm.network(3,2)
nw.A=np.array([[-1.,0.46 ,0.],
            [0.3 ,-1.,0.33], 
            [0.,  0.26,-1.]])
nw.set_C(0,0,0.26)
nw.set_C(1,2,0.01)
nw.set_D(2,0,1,30.)
#dcm.simulation(nw,t_end,u,h1,h2,'quadr_2')


u=lambda t: np.array([gaussian(t,5,30)+gaussian(t,20,15)+gaussian(t,35,7.5)])
t_end=50

nw=dcm.network(2,1)
nw.set_A(0,1,0.5)
nw.set_A(1,0,0.5)
nw.set_D(0,0,1,5.)
nw.set_C(0,0,1.)
nw.set_u(u)
max1=[]
breite=[]
#dcm.simulation(nw,t_end,h1,h2,'nw(2)-onetime')

#for i in range(25):
#    print(i)
#
#    sigma=4.5+0.8*i
#    
#    u=lambda t: np.array([gaussian(t,5,sigma)])
#    nw.set_u(u)
#
#    data=dcm.simulation(nw,t_end,u,h1,h2,'quadr_3')
#    bold_list=data[1]
#    
#    breite.append(1./sigma)
#    max1.append(max(bold_list[1][1]))
#    
#    print(breite[-1], max1[-1])
#    
#plt.plot(breite, max1)